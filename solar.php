<!DOCTYPE html>

<?php
	//phpinfo();
	include("functions.php");
?>
<?php
	//*********************************************************************************************
	// Letzten Solargrafik-File suchen und für Auswahlfeld merken
	//*********************************************************************************************
	
	$verz  = opendir("./");
	$JM_a  = 200501;			// Aktuelle Jahr/Monat-Kombinationszahl
	
	while ($file = readdir($verz))
	{
		if (sscanf ($file, "solargrafik_%06d", $JM) == 1);
		{
			//echo "File: ".$file."=> JM: ".$JM;
			if ($JM > $JM_a) $JM_a  = $JM;
		}
	}
	
	$Jahr = (int)($JM_a / 100);
	$Monat = (int)($JM_a - ($Jahr * 100));
?>
<html lang="de">
	<head>
		<link rel="shortcut icon" href="favicon.ico" />
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Hans-Peter & Christoph H&auml;fner">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/main.css">
		<title>Solar B&uuml;rgerAktiv Hildrizhausen GbR / Photovoltaik Hildrizhausen GbR</title>
		<!-- Piwik -->
		<script type="text/javascript">
		  var _paq = _paq || [];
		  _paq.push(["trackPageView"]);
		  _paq.push(["enableLinkTracking"]);

		  (function() {
			var u=(("https:" == document.location.protocol) ? "https" : "http") + "://homelinux.christophhaefner.de/piwik/";
			_paq.push(["setTrackerUrl", u+"piwik.php"]);
			_paq.push(["setSiteId", "2"]);
			var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
			g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
		  })();
		</script>
		<!-- End Piwik Code -->
	</head>
	<body>
		<div id="wrap">
		<!-- Page Content -->
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2" id="content">
				<div id="contentWrapper">
					<div class="row highlight">
						<div class="col-md-12">
							<img src="Solaranlage.jpg" alt="Solaranlage Schönbuchhalle Hildrizhausen" class="img-responsive center-block">
						</div>
					</div>
					<div class="row" id="zeileAnlageHalle">
						<div class="col-sm-6 col-xs-8 highlight" id="anlageHalle">
							<p>
								<u>Anlage Schönbuchhalle:</u><br /><br />
								Inbetriebnahme: 16.12.2004<br />
								Spitzenleistung: 29,08 kWp<br />
								Größe: 250 m²<br />
								Modulanzahl: 183
							</p>
						</div>
						<div class="col-sm-3 col-xs-4">
							<img src="LogoSBAH.gif" alt="Logo Solar BürgerAktiv Hildrizhausen GbR" id="iconSolarBuergerAktiv" class="img-responsive center-block">
						</div>
						<div class="clearfix visible-xs"></div>
						<div class="col-sm-3 col-xs-12">
							<div class="inputWrapper">
								<?php include('include_form.php'); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 hidden-xs platzhalter">
							<!-- Platzhalter -->
						</div>
					</div>
					<div class="row highlight">
						<div class="col-md-12">
							<img src="Photovoltaik.jpg" alt="Solaranlage Schönbuchsaal+Schule Hildrizhausen" class="img-responsive center-block">
						</div>
					</div>
					<div class="row" id="zeileAnlageSaal">
						<div class="col-sm-6 col-xs-8 highlight" id="anlageSaal">
							<u>Anlage Schönbuchsaal+Schule:</u><br /><br />
							Inbetriebnahme: 22.11.2008<br />
							Spitzenleistung: 30,0 kWp<br />
							Größe: 291 m²<br />
							Modulanzahl: 400<br />
						</div>
						<div class="col-sm-3 col-xs-4">
							<img src="LogoPV_H.gif" alt="Photovoltaik Hildrizhausen GbR" id="iconPhotovoltaik" class="img-responsive center-block">
						</div>
						<div class="clearfix visible-xs"></div>
						<div class="col-sm-3 col-xs-12">
							<div class="inputWrapper">
								<?php include('include_form.php'); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 hidden-xs platzhalter">
							<!-- Platzhalter -->
						</div>
					</div>
					<div class="row highlight">
						<div class="col-xs-8">
							<p>Dipl.-Ing.(FH) H.-P H&auml;fner<br />Christoph H&auml;fner</p>
						</div>
						<div class="col-xs-4" id="stand">
							<p>Stand: 06.01.2014</p>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
		</div>
	</body>
</html>
