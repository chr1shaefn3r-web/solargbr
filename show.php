<!doctype html>
<?php include("functions.php"); ?>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<link rel="shortcut icon" href="favicon.ico" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Hans-Peter & Christoph H&auml;fner">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/main.css">
		<title>Solar B&uuml;rgerAktiv Hildrizhausen GbR / Photovoltaik Hildrizhausen GbR</title>
	</head>
	<body>
		<div id="wrap">
		<!-- Page Content -->
		<div class="container">
			<div class="row">
				<div class="col-md-1">
					<a href="index.php">HOME</a>
				</div>
				<div class="col-md-10">
<?php
	$Jahr = $_GET['Jahr'];
	$Monat = $_GET['Monat'];
	
	$Monat_vorher = $Monat - 1;
	$Jahr_vorher = $Jahr;
	
	if ($Monat_vorher == 0)
	{
		$Monat_vorher = 12;
		$Jahr_vorher = $Jahr - 1;
	}
	
	$Monat_nachher = $Monat + 1;
	$Jahr_nachher = $Jahr;
	
	if ($Monat_nachher > 12)
	{
		$Monat_nachher = 1;
		$Jahr_nachher = $Jahr + 1;
	}

	$Jahr_after = $Jahr + 1;
	$Jahr_befor = $Jahr - 1;
?>
					<?php
						if (imageExists($Jahr, $Monat))
						{
							$src = fileNameFor($Jahr, $Monat);
							$alt = imageDescriptionFor($Jahr, $Monat);
						}
						else
						{
							$src = "solargrafik_000000.png";
							$alt = "Keine Daten vorhanden!";
						}
					?>
					<?php
					echo '<img class="img-responsive center-block" src="'.$src.'" alt="'.$alt.'" />';
					?>
					<ul class="pager">
						<?php echo'
						<li class="'.disabled($Jahr_after, $Monat).'">
							'.linkTo($Jahr_after, $Monat, leftArrow("&uarr;"), rightArrow("&uarr;")).'
						</li>';
						?>
					</ul>
					<ul class="pager">
						<?php	echo '
						<li class="'.disabled($Jahr_vorher, $Monat_vorher).'">
							'.linkTo($Jahr_vorher, $Monat_vorher, leftArrow("&larr;")).'
						</li>';
						?>
						<li class="hidden-cs" id="current">
							<?php
							echo '<span>'.translatedigit2char($Monat).' '.$Jahr.'</span>';
							?>
						</li>
						<?php echo'
						<li class="'.disabled($Jahr_nachher, $Monat_nachher).'">
							'.linkTo($Jahr_nachher, $Monat_nachher, "", rightArrow("&rarr;")).'
						</li>';
						?>
					</ul>
					<ul class="pager">
						<?php echo'
						<li class="'.disabled($Jahr_befor, $Monat).'">
							'.linkTo($Jahr_befor, $Monat, leftArrow("&darr;"), rightArrow("&darr;")).'
						</li>';
						?>
					</ul>
				</div>
			</div>
		</div>
		</div>
	</body>
</html>
