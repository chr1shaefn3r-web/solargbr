<?php
function fileNameFor($year, $month)
{

	return sprintf ("solargrafik_%04s%02s.png", $year, $month);
}

function imageExists($year, $month)
{
	return file_exists(fileNameFor($year, $month));
}

function imageDescriptionFor($year, $month)
{
	return "Solarstatistik ".translatedigit2char($Monat)." ".$Jahr;
}

function linkTo($year, $month, $slotBefor = "", $slotAfter = "")
{
	return '<a href="show.php?Jahr='.$year.'&Monat='.$month.'">'.$slotBefor.''.translatedigit2char($month).'&nbsp;'.$year.''.$slotAfter.'</a>';
}

function leftArrow($arrow) {
	return $arrow."&nbsp;";
}
function rightArrow($arrow) {
	return "&nbsp;".$arrow;
}

function disabled($year, $month)
{
	if(imageExists($year, $month))
	{
		return "";
	}
	else
	{
		return " disabled";
	}
}

function translatedigit2char ($param)
{
    switch ($param)
    {
        case 1:
            $monat = 'Januar'; break;
        case 2:
            $monat = 'Februar'; break;
        case 3:
            $monat = 'März'; break;
        case 4:
            $monat = 'April'; break;
        case 5:
            $monat = 'Mai'; break;
        case 6:
            $monat = 'Juni'; break;
        case 7:
            $monat = 'Juli'; break;
		case 8:
            $monat = 'August'; break;
		case 9:
            $monat = 'September'; break;
		case 10:
            $monat = 'Oktober'; break;
		case 11:
            $monat = 'November'; break;
		case 12:
            $monat = 'Dezember'; break;
    }
    return $monat;
}
function translateeng2de ($param)
{
    switch ($param)
    {
        case "January":
            $monat = 'Januar'; break;
		case "February":
            $monat = 'Februar'; break;
        case "March":
            $monat = 'März'; break;
        case "April":
            $monat = 'April'; break;
        case "May":
            $monat = 'Mai'; break;
        case "June":
            $monat = 'Juni'; break;
        case "July":
            $monat = 'Juli'; break;
		case "August":
            $monat = 'August'; break;
		case "September":
            $monat = 'September'; break;
		case "October":
            $monat = 'Oktober'; break;
		case "November":
            $monat = 'November'; break;
		case "December":
            $monat = 'Dezember'; break;
	}
    return $monat;
}
?>
